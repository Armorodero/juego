public class Acciones {

	public static void main(String[] args) {
			
		Mago a = new Mago("Merlin", 38123456, 40, "Curar");
		a.getNombre();
		a.getMagia();
		
		Espadachin b = new Espadachin("Leonidas", 47789456, 30, "Pegar");
		b.getNombre();
		b.getFuerza();
		b.getEnergia();
		
		Espadachin c = new Espadachin("Ares", 41654356, 300, "Pegar");
		c.getNombre();
		c.getFuerza();
		
		Enano d = new Enano("Portos", 45678912, 45, "Flechaso");
		d.getNombre();
		d.getAgilidad();
		
		a.Moverse(a);
		a.Pensar(b, c, d);
		
		b.Pegarconarma(c);
		d.Manejoarco(c);
		c.Pegarconarmas(b);
		a.Curar(b);
		
	}

}
