public class Enano extends Humano {

	public Enano(String nombre, int dni, int energia, String agilidad) {
		super(nombre, dni, energia);
		Agilidad = agilidad;
	}
	
	String Agilidad;

	public String getAgilidad() {
		return Agilidad;
	}

	public void setAgilidad(String agilidad) {
		Agilidad = agilidad;
	}
	
	void Manejoarco(Espadachin c) {
		System.out.println("Hit " + c.getNombre());
		System.out.println(c.getEnergia());
		c.Energia --;
		System.out.println(c.getEnergia());
	}
}
