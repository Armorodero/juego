public class Personaje {

	String Nombre;
	
	public String getNombre() {
		return Nombre;
	}

	public void setNombre(String nombre) {
		Nombre = nombre;
	}

	public Personaje(String nombre) {
		super();
		Nombre = nombre;
	}

	void Moverse(Mago a) {
		System.out.println(a.getNombre() + " se esta moviendo");		
	}
}
