public class Humano extends Personaje {

	public Humano(String nombre, int dni, int energia) {
		super(nombre);
		DNI = dni;
		Energia = energia;
	}

	int DNI;
	int Energia;

	public int getDNI() {
		return DNI;
	}

	public void setDNI(int dNI) {
		DNI = dNI;
	}

	public int getEnergia() {
		return Energia;
	}

	public void setEnergia(int energia) {
		Energia = energia;
	}

	void Pensar(Espadachin b, Espadachin c, Enano d) {
		System.out.println(b.getNombre() + " esta pensando");
		System.out.println(c.getNombre() + " esta pensando");
		System.out.println(d.getNombre() + " esta pensando");
	}
}
