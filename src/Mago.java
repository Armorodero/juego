public class Mago extends Humano {
	
	public Mago(String nombre, int dni, int energia, String magia) {
		super(nombre, dni, energia);
		Magia = magia;
	}

	String Magia;
	
	public String getMagia() {
		return Magia;
	}

	public void setMagia(String magia) {
		Magia = magia;
	}

	void Curar(Espadachin b) {
		System.out.println("Curar a " + b.getNombre());
		b.Energia ++;
		System.out.println("Energia " + b.getEnergia());
	}

}
