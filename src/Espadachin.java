public class Espadachin extends Humano {
	
	public Espadachin(String nombre, int dni, int energia, String fuerza) {
		super(nombre, dni, energia);
		Fuerza = fuerza;
	}

	String Fuerza;
	
	public String getFuerza() {
		return Fuerza;
	}

	public void setFuerza(String fuerza) {
		Fuerza = fuerza;
	}

	void Pegarconarmas(Espadachin b) {
		System.out.println("Hit " + b.getNombre());
		System.out.println(b.getEnergia());
		b.Energia --;
		System.out.println(b.getEnergia());
	}
		
	void Pegarconarma(Espadachin c) {
		System.out.println("Hit " + c.getNombre());
		System.out.println(c.getEnergia());
		c.Energia --;
		System.out.println(c.getEnergia());
	}
}
